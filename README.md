# Hacking the Python interview

Slides for the pub quiz on hacking the Python interview:

https://python-sprints.github.io/london/2019/04/10/hacking-python-interview.html

## Set up

- Install [Miniconda 3.7](https://docs.conda.io/en/latest/miniconda.html)
- Open an Anaconda/UNIX terminal
- `git clone https://github.com/datapythonista/hacking_python_interview.git`
- `cd hacking_python_interview`
- `conda env create`
- `source activate hacking_python_interview` (in Windows: `conda activate hacking_python_interview`)
- `jupyter notebook`
- Open `Hacking Python interview.ipynb` notebook
- Click the icon with the bar plot to show as slides with [RISE](https://damianavila.github.io/RISE/)
